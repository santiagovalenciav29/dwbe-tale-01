require('dotenv').config();
const express = require('express');
const app = express();

const PORT = process.env.PORT || 3000;

app.use(express.static('./src/public'));
app.use(express.json());

const lista_usuarios = ["lista", "lista2", "lista3"]

//para obtener usuarios
app.get('/usuarios', (req,res) => {
    res.json(lista_usuarios);
})

//para crear usuarios
app.post('/usuarios', (req, res) => {
    // const { id } = req.body;
    const { nombre } = req.body;
    // const { edad }= req.body;
    lista_usuarios.push(nombre);
    res.json('usuario creado exitosamente');
});

//para actualizar usuarios
app.put('/usuarios',(req, res)=> {
    const { index } = req.query;
    const { nombre } = req.body;
    lista_usuarios[index] = nombre;
    res.json('Usuario actualizado');
})
// app.get('/', (req,res) => {
//     res.send('cliente digameeeee')
// });
//para borrar usuarios
app.delete('/usuarios', (req,res) => {
    const { index } = req.query;
    lista_usuarios.splice(index, 1);
    res.json('usuario eliminado')
})

app.listen(PORT, () => {
    console.log('escuchando desde el puerto ' + PORT + ' pummmmm !!');
});