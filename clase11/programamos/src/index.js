const express = require('express');
const app = express();
const { precioBajoTelefonos, obtenerTelefonos, precioAltoTelefonos, agrupacionGamas } = require('./models/telefonos.js');

app.get('/telefonos', (req, res) => {
    res.json(obtenerTelefonos());
});

app.get('/precioBajotelefonos', (req, res) => {
    res.json(precioBajoTelefonos());
});

app.get('/precioAltoTelefonos', (req, res) => {
    res.json(precioAltoTelefonos());
});

app.get('/agrupacionGamas', (req, res) => {
    res.json(agrupacionGamas());
});

app.listen(3000, () => {
    console.log('Escuchando en el puerto 3000');
});
