// const { json } = require('express');
const express = require('express');
const app = express();
const morgan = require('morgan');

// midelware es un manejador de peticion que nosotros podemos ejecutar 
// antes de que lleguen a su ruta original, es como el app.all pero este no
//sirve para una sola ruta, sirven para todas. DEBE IR ANTES DE LAS RUTAS
// app.use(logger);

app.use(morgan('dev'));

// function logger(req, res, next) {
//     console.log(`route received: ${req.protocol}://${req.get('host')}${req.originalUrl}`);
//     next();
// }


//para que express me entienda los objetos json cuando la app se los envie
app.use(express.json());
// en el caso del send se puede remplazar por un objeto json

// el app.all se le pone una ruta y el entiende que todas las rutas que vayan
//por ahi primero deben pasar por el app.all
app.all('/user', (req, res, next) => {
    console.log('por aqui paso algo');
    // res.send('finish');
    next();
});


app.get('/user', (req, res) => {
    res.json({
        username : 'adelita',
        lastname : 'del Toro'
    });
})

//para ver algo que me este mandando el cliente desde la app
//en este caso postman, puedo hacerlo con un objeto de javascript
// llamado req.body te muestra la informacion que el cliente envia
//tambien se pueden crear rutas dinamicas con el /: y verlo con 
//req.params te da cierta informacion de determinados recursos

app.post('/user/:id', (req, res) => {
    console.log(req.body);
    console.log(req.params);
    res.send('POST REQUEST RECEIVED');
});

app.put('/contact/:id', (req, res) => {
    console.log(req.body);
    res.send(`user ${req.params.id} updated`);
});

app.delete('/user/:userId', (req, res) =>{
    res.send(`user ${req.params.userId} deleted`)
})

app.use(express.static('public'));

app.listen(3000, function () {
    console.log('Escuchando el puerto 3000!');
  });