let express = require('express');
let app = express();
const fs = require('fs');

let estudiantes = ['Nombre :Lina edad :23', 'Nombre :pepe edad :32', 'Nombre :Isa edad :22', 'Nombre pipe: edad :24'];

estudiantes.forEach((estudiante, i) => {
  const est = `estudiante  ${ i + 1 } => ${ estudiante }\n`;
  fs.appendFileSync('estudiantes.txt', est, function (err) {
    if(err) console.log(err);
    else console.log('done!');
   });
});



app.get('/estudiantes', function (req, res) {
  res.send(estudiantes);
});

app.listen(3000, function () {
  console.log('Escuchando el puerto 3000!');
});