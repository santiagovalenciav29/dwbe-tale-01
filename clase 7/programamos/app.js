class Persona {
    constructor(nombre, apellido, edad){
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
    }

    fullName() {
        return this.nombre + ' ' + this.apellido
    }

    esMayor() {
        return this.edad >= 18;
    }
}

let persona1 = new Persona ('ramiro', 'cazas', 23)

console.log ( persona1.fullName())

console.log ( persona1.esMayor())

// crea una nueva persona con un objeto vacio y crea todo lo
// de forma manual