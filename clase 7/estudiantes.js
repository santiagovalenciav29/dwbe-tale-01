class student {
    constructor (name, email, program) {
        this.name = name;
        this.email = email;
        this.program = program;
    }

    talk () {
       console.log(`hello, I´m : ${this.name} , nice to meet you`)
    }

    walk() {
        console.log(`time to move, they're waiting for us in the another room`)
    }

    participate() {
        console.log(`about the document we read, I think we could include this information in
        our project, what do you think? `)
    }
}

let student1 = new student ("pepito", "pepito@gmail.com", "science");

student1.talk();