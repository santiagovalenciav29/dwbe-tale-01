// para crear un modulo se usa el require
// todo lo que se haga con fs se realizara de forma asincrona

const fs = require('fs');

function leer(ruta, cb) {
    fs.readFile(ruta, (err, data) => {
       cb(data.toString());
    })
}

// leer(__dirname + '/archivo.txt', console.log)

function write(route, content, cb) {
    fs.writeFile(route, content, function (err) {
        if(err) {
            console.error('I could not write it', err);
        }else{
            console.log('it has been written correctly')
        }
    });
}

// write(__dirname + '/archivo1.txt', 'I am a new file ', console.log)

function clear(route,cb) {
    fs.unlink(route, cb);
}

clear(__dirname + '/archivo1.txt', console.log)