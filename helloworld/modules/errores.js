function otraFuncion() {
    return seRompe();
}

function seRompe() {
    return 3 + z;
}

function seRompeAsincrona(cb) {
    setTimeout(function () {
        try {
            return 3 + z;
        } catch (err) {
            console.error('Error en la funcion asincrona');
            cb(err);
        }
        
    })
}

try{
    // seRompe();
    // otraFuncion();
    seRompeAsincrona( function (err) {
        console.log(err.message);
    })
} catch(err) {
    console.error('revisemos, hay algo mal ejecutado');
    console.error(err.message);
    //con el console.error(err) me explica cual es la fuente del error
    //con el console.error(err.message) nos arroja el mensaje del porque del error y continua
}

console.log('apesar del error el programa continua funcionando')
