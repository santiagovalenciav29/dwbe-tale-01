function hola (nombre,miCallback) {
    setTimeout(function() {
         console.log('salut, ' + nombre + '  on teste des callbacks');
         miCallback(nombre);
    }, 2000)
}

function hablar (callBackHablar) {
    setTimeout(function(){
        console.log('bla bla bla bla....');
        callBackHablar();
    },2000)
}

function adios (nombre, otroCallback) {
    setTimeout(function() {
        console.log('au-revoir', nombre);
        otroCallback();
    }, 3000)
}

function conversacion(nombre, veces, callback) {
    if(veces > 0) {
        hablar(function () {
            conversacion(nombre, --veces, callback);
        })
    } else {
        adios(nombre, callback);
    }
    
}

console.log('en commencant ')

hola('Santiago', function(nombre){
    conversacion(nombre, 5, function () {
        console.log('processus complet');
    });
})

// hola('santiago', function (nombre){
//     hablar(function () {
//         adios(nombre, function (){
//             console.log('terminando desde la asincronia')
//          });
//     });
// });



