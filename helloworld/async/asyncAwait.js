// poniendo async al inicio declaramos la funcion como asincrona 

async function hola (nombre) {
    return new Promise( (resolve, reject) => {
        setTimeout(function() {
            console.log('salut, ' + nombre + '  on teste des callbacks');
            resolve(nombre);
       }, 2000);
    });
    
}

async function hablar (nombre) {
    return new Promise((resolve, reject) => {
        setTimeout(function(){
            console.log('bla bla bla bla....');
            resolve(nombre);
            reject('algo esta fallando');
        },2000)
    })
    
}

async function adios (nombre) {
    return new Promise( function (resolve, reject) {
        setTimeout(function() {
            console.log('au-revoir', nombre);
            resolve(nombre);
        }, 3000);
    });
   
}

async function main() {
   let nombre = await hola ('Santiago');
    await hablar();
    await hablar();
    await hablar();
    await adios(nombre);
    console.log('la fin')
}

console.log('en commencant')

main();

// con el async, await podemos coordinar el orden de ejecucion de nuestras funciones 