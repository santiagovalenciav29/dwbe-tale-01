function hola (nombre) {
    return new Promise( (resolve, reject) => {
        setTimeout(function() {
            console.log('salut, ' + nombre + '  on teste des callbacks');
            resolve(nombre);
       }, 2000);
    });
    
}

function hablar (nombre) {
    return new Promise((resolve, reject) => {
        setTimeout(function(){
            console.log('bla bla bla bla....');
            resolve(nombre);
            reject('algo esta fallando');
        },2000)
    })
    
}

function adios (nombre) {
    return new Promise( function (resolve, reject) {
        setTimeout(function() {
            console.log('au-revoir', nombre);
            resolve(nombre);
        }, 3000);
    });
   
}

console.log('Iniciando el proceso...')
hola('Santiago')
    // .then(nombre => {
    //     return adios(nombre);
    // })
    
    .then(hablar)
    .then(adios)
    .then((nombre) => {
        console.log('Terminando el proceso..')
    })
    .catch(error => {
        console.error('algo no esta funcionando bien')
        console.error(error);
    })