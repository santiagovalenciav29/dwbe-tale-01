function sumar (n1, n2) {
    return n1+n2;
}

function restar (n1, n2) {
    return n1-n2;
}

function dividir (n1, n2) {
    return n1/n2;
}

function multiplicar (n1, n2) {
    return n1*n2;
}

exports.sumar = sumar;
exports.restar = restar;
exports.dividir = dividir;
exports.multiplicar = multiplicar;