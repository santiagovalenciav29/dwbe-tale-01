/*export*/ function hablar(texto) {
    console.log(texto);
}

/*export*/ function hablar_bajo(texto) {
    console.log(texto.toLowerCase());
}

exports.hablar = hablar;
exports.hablar_bajo = hablar_bajo;

// exports.modules = (hablar, hablar_bajo)