// let es_par = function (numero){
//     if(numero % n2 === 0)
// {
//     return true;
// }else{
//     return false;
// }
// }

let es_par = numero => numero % 2 === 0; //? true : false por ser booleano podria escribirse asi

document.getElementById('btn').addEventListener('click', () => {
    const numero = parseInt(document.getElementById("numero").value);
    const mensaje = es_par(numero) ? "es par" : "es impar";
    document.getElementById('resultado').innerHTML = mensaje;
})