

const usuarios = [
    {
        id : 1,
        usuario: "Santiago",
        email:"sa@gmail.com",
        username:"tiago",
        password:"1234",
        telefono: 321983457,
        direccion:"av. siempre viva ",
        isAdmin:true
    },
    {
        id : 2,
        usuario: "Evangelino",
        email:"ev@gmail.com",
        username:"Eve",
        password:"5678",
        telefono:3004356437,
        direccion:"av. candeleo",
        isAdmin:false
    }

];

const obtenerUsuarios = () => {
    return usuarios
}

const nuevoUsuario = (usuario) => {
    usuarios.push(usuario);
};

module.exports = {obtenerUsuarios, nuevoUsuario }