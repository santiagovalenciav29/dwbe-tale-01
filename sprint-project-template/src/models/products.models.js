const Products = [
        {
            id : 1,
            productName: "TraditionalBurguer",
            price:10,
            q: 1
        },
        {
            id : 2,
            productName: "mexicanBurguer",
            price:13,
            q: 1
        },
        {
            id : 3,
            productName: "cheeseBurguer",
            price:15,
            q:1
        },
        {
            id : 4,
            productName: "especialBurguer",
            price:15,
            q:1
        },
        {
            id : 5,
            productName: "limonade",
            price:2,
            q:1
        },
        {
            id : 6,
            productName: "naturalJuice",
            price:3,
            q:1
        },
        {
            id : 7,
            productName: "softDrink",
            price:3,
            q:1
        },
    ];

const getProducts = () => {
     return Products;
};

const pushProducts = (product) => {
    Products.push(product);
};

module.exports = { getProducts , pushProducts };