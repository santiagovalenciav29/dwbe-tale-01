const pedidos = [
    {
        id : 1,
        orderCost: 0,
        username:"tiago",
        direccion: "av.siempre viva",// user linked to the order
        products:[],
        medioDePago:"efectivo",
        state: 'pendiente',
    },
    {
        id : 2,
        orderCost: 0,
        username:"Eve",
        direccion: "av. candeleo",
        products:[],
        medioDePago:"tarjeta",
        state: 'pendiente',
    },
];

const estadosUser = [
    {
        "id": 2,
        "state": "confirmado"
    },
]

const estadosAdmin = [
    {
        "id": 1,
        "state": "pendiente"
    },
    {
        "id": 2,
        "state": "confirmado"
    },
    {
        "id": 3,
        "state": "en preparacion"
    },
    {
        "id": 4,
        "state": "enviado"
    },
    {
        "id": 5,
        "state": "entregado"
    }
]

const orders = () => {
    return pedidos;
};

const getStatesUser = () => {
    return estadosUser;
};

const getStatesAdmin = () => {
    return estadosAdmin;
};

const nOrder = (product) => {
    pedidos.push(product);
}

function objetoAmodificar(id, orderCost, username, direccion, products, medioDePago, state) {
   return {
        id : id,
        orderCost: orderCost,
        username:username,
        direccion: direccion,
        products: products,
        medioDePago: medioDePago,
        state: state
       }
}

module.exports = {orders,objetoAmodificar, nOrder, getStatesUser, getStatesAdmin};