const { pushProducts } = require("./products.models");

const payments = [
    {
        id: 1,
        medioDePago:"efectivo"
    },
    {
        id:2,
        medioDePago:"paypal"
    },
    {
        id:3,
        medioDePago:"tarjeta"
    }
];

const getPayments = () => {
    return payments;
}

const addPayments = (newPayment) => {
    const id = payments.length+1;
    const pay = {
        id : id,
        medioDePago: newPayment
    }
   return payments.push(pay);
}

module.exports = { getPayments, addPayments};