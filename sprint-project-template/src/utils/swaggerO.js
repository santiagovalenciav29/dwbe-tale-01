const swaggerOptions = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "Sprint project 1 - Protalento",
            version: "1.0.0",
            description: "Proyecto 1 para acamica DWBE",
            contact : {
                name : " Santiago Valencia Valencia",
                email : "santiagovalenciav29@gmail.com"
            }
        },
        servers: [
            {
                url: "http://localhost:5000",
                description: "Servidor de prueba"
            }
        ],
        components: {
            securitySchemes: {
                basicAuth: {
                    type: "http",
                    scheme: "basic"
                }
            }
        },
        security: [
            {
                 basicAuth: []
            }
        ]
    },
    apis: ["./src/routes/*.js"]
};

module.exports = swaggerOptions;
