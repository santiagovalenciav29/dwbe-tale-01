const courses = [
    {
        Name: "frontend",
        id: 1
    },
    {
        Name: "backend",
        id: 2
    },
    {
        Name:"UX/UI",
        id: 3
    },
    {
        Name: "Data analyst",
        id: 4
    },
]

const options = () => {
    return courses;
}

module.exports = { options }