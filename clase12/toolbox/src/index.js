const express = require('express');
const app = express();


const { options } = require('./models/courses.js');

const { logger, loggin } = require('./models/logger.js');

app.use(express.json());

app.use(logger);

app.get('/courses', (req, res) => {
    res.send(options())
  });

app.post('/estudiantes', (req, res) => {
    res.status(201).send();
  });

app.post('/loggin', loggin, (req, res) => {
    res.send([
      {id: 1, nombre: "Lucas", edad: 35}
    ])
  });

app.listen(5000, () => console.log("listening on 5000"));