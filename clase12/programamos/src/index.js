const express = require('express');
const app = express();

app.use(express.json());

const usuarios = [
    { id: 1 , nombre: "pepe", email:"pepe@gmail.com" },
    { id: 2 , nombre: "eva", email:"eva@gmail.com" },
    { id: 3 , nombre: "don", email:"don@gmail.com" }
    
]

app.use((req, res, next) => {
    console.log(req.url);
    next();
});

app.get('/usuarioporid/:id', (req, res) => {
    const id = req.params.id;
    const usuario = usuarios.find(u => u.id ==id);
    if(usuario) {
        res.json(usuario)
    } else {
        res.status(404).json('usuario no encontrado');
    }
});

app.get('/usuarioporNombre/:nombre', (req, res) => {
    const nombre = req.params.nombre;
    const usuario = usuarios.find(u => u.nombre ==nombre);
    if(usuario) {
        res.json(usuario)
    } else {
        res.status(404).json('usuario no encontrado');
    }
});

app.listen(3002);