const express = require('express');
const app = express();
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

const swaggerOptions = require('./utils/swaggerO.js');

app.use(express.json());

const swaggerSpecs = swaggerJsDoc(swaggerOptions);

app.use('/api-docs',
        swaggerUI.serve,
        swaggerUI.setup(swaggerSpecs));



app.use('/', require('./routes/autores.routes.js'));

app.listen(3000, () => {
    console.log("vamos!!!!!!!! library")
});