const express = require('express');
const router = express.Router();
const {obtenerAuthors, nuevoAutor} = require("../models/authors.models.js");

/**
 * @swagger
 * /authors:
 *  get:
 *     summary: Obtener todos los usuarios del sistema
 *     tags: [Usuarios]
 *     responses:
 *        200:
 *           description: Lista de usuarios del sistema
 *           content:
 *           application/json:
 *              schema:
 *                  type:array
 *                  items:
 *                      $ref: '#/components/schemas/usuario'
 */
router.get('/authors', (req, res) => {
    res.json(obtenerAuthors());
});

router.post('/authors', (req, res) => {
    const { id, nombre , apellido, fechaDeNacimiento, libros } = req.body;
    if (id && nombre && apellido && fechaDeNacimiento && libros) {
        const autor = nuevoAutor(req.body);
        res.json(autor);
    }else {
        res.json('no se pudo crear el autor');
    }
});

router.get('/authorsbyid/:id', (req, res) => {
    const real = obtenerAuthors().some(r => r.id === parseInt(req.params.id));
    if (real){
        res.json(obtenerAuthors().find(a => a.id === parseInt(req.params.id)));
    } else {
        res.status(400).json('id desconocido');
    }
    
});

router.put('/authorsbyid/:id', (req, res) => {
    const real = obtenerAuthors().some(r => r.id === parseInt(req.params.id));
    if (real){
        const nAuthor = req.body;
        obtenerAuthors().forEach(author => {
         if (author.id === parseInt(req.params.id)){
            author.nombre = nAuthor.nombre ? nAuthor.nombre : author.nombre;
            author.apellido = nAuthor.apellido ? nAuthor.apellido : author.apellido;
            res.json('datos actualizados');
        }
    });
    } else {
        res.status(400).json('id desconocido');
    }
    
});

router.delete('/authorsbyid/:id', (req, res) => {
    const real = obtenerAuthors().some(r => r.id === parseInt(req.params.id));
    if(real){
        res.json(obtenerAuthors().filter(a => a.id !== parseInt(req.params.id)));
    } else {
        res.status(400).json('id desconocido');
    }
    
});

router.get('/authorsbyid/:id/librosbyid/:idlibro', (req, res) => {
    const {id, idlibro } = req.params;
    const realAuthor = obtenerAuthors().find(r => r.id === parseInt(id));
    realAuthor ? res.json (realAuthor.libros.find(b => b.id === parseInt(idlibro)))
               : res.status(404).json('autor no encontrado');
});

module.exports = router;