const Autores = [
    {
        id: 1,
        nombre: "Jorge Luis",
        apellido: "Borges",
        fechaDeNacimiento: "24/08/1899",
        libros: [
            {
                id: 1,
                titulo: "Ficciones",
                descripcion: "Se trata de uno de sus mas...",
                anioPublicacion: 1944
            },
            {
                id: 2,
                titulo: "El Aleph",
                descripcion: "Se trata de uno de sus mas...",
                anioPublicacion: 1949
            },
    ]   
   },
   {
        id: 2,
        nombre: "Jorgy",
        apellido: "Bill",
        fechaDeNacimiento: "20/08/1899",
        libros: [
            {
                id: 1,
                titulo: "Horror",
                descripcion: "Se trata de uno de sus mas...",
                anioPublicacion: 1941
            },
            {
                id: 2,
                titulo: "El caso",
                descripcion: "Se trata de uno de sus mas...",
                anioPublicacion: 1943
            }
        ]
   }
]

const obtenerAuthors = () => {
    return Autores;
}

const nuevoAutor = (autor) => {
   Autores.push(autor);
}


module.exports = { obtenerAuthors, nuevoAutor  }