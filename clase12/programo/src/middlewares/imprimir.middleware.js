function imprimirRequest(req, res, next){
    console.log("hora desde middleware ", Date.now());
    next();
}

function imprimirRequest2(req, res, next){
    console.log("Path desde middleware ", req.path);
    if(req.path === "/ejemplo2"){
        next();
    }else {
      res.json('no autorizado');
    };
}

function imprimirRequest3(req, res, next){
    console.log("Path desde middleware ", req.path);
    res.status(401).json("no estas autorizado");
}

module.exports = { imprimirRequest, imprimirRequest2, imprimirRequest3 }