var moment = require('moment');

console.log("horario local : " + moment().format("dddd, MMMM Do YYYY, h:mm:ss a"));

const diferencia = moment().hour() - moment.utc().hour();

console.log("la diferencia es " + (24 - diferencia) + " horas");

console.log("horario UTC : " + moment.utc().format("dddd, MMMM Do YYYY, h:mm:ss a"))

const comparacion = moment('2017-06-20').isBefore('2019-03-15')

if(comparacion) {
    console.log('efectivamente la fecha 2017-06-20 es anterior a 2019-03-15')
}else{
    console.log('la fecha 2019-03-15 es anterior a 2017-06-20 ')
}