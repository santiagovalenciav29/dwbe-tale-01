require('dotenv').config();
//consultar basic-auth library
// las variables de entorno se usan para saber en el mismo codigo en que ambiente esta 
// y swagger 
const env = require('./appsettings.json')
const chalk = require('chalk');

console.log(chalk.blue('Hello world'));

const node_env = process.env.NODE_ENV || 'QA';
const variables = env[node_env];

console.log(chalk.blue(node_env));
console.log(chalk.red(JSON.stringify(variables)));

//las variables de conexion se guardan en .env y el usuario y 
//contraseña encriptados