//El arreglo donde guardaremos los usuarios
let usuarios = new Array();
//TODA ESTA PARTE ES PARA EL REGISTRO
//Añadimos escucha al botón registrarse y ejecutamos su función
document.getElementById("registrarse").addEventListener('click', (e) => registrarse(e));
//Función que se ejecuta en el evento click al registrarse
function registrarse(e) {
    const nombre = document.getElementById("nombreRegistro").value;
    const apellido = document.getElementById("apellidoRegistro").value;
    const email = document.getElementById("emailRegistro").value;
    const paisRegistro = document.getElementById("paisRegistro").value;
    const password = document.getElementById("passwordRegistro").value;
    const passwordConfirm = document.getElementById("passwordConfirmRegistro").value;
    if (nombre && apellido && email && paisRegistro && password && passwordConfirm) {
        if (password === passwordConfirm) {
            if (validarCorreo(email)) {
                let usuario = {
                    nombre: nombre,
                    apellido: apellido,
                    correo: email,
                    pais: paisRegistro,
                    password: password
                }
                usuarios.push(usuario);
                console.log(usuario);
                alert("Usuario registrado con éxito");
            } else {
                alert("El usuario ya fue registrado");
            }
            e.preventDefault();
        } else {
            alert("Las contraseñas no coinciden");
            e.preventDefault();
        }
        limpiarFormulario(1);
    }
};
//Función auxiliar para validar un correo si existe en Usuarios
function validarCorreo(correo) {
    let correoValido = true;
    usuarios.forEach(usuario => {
        if (usuario.correo == correo) {
            correoValido = false;
        }
    });
    return correoValido;
}
//TODA ESTA PARTE ES PARA EL LOGIN
//Añadimos escucha al botón iniciar sesión y ejecutamos su función
document.getElementById("iniciarSesion").addEventListener('click', (e) => iniciarSesion(e));
//Funcion que se ejecuta con el evento de iniciar sesion
function iniciarSesion(e) {
    let indiceUsuario = 'false';
    let correo = document.getElementById("correoLogin").value;
    let password = document.getElementById("passLogin").value;
    if (correo && password) {
        usuarios.forEach(usuario => {
            if (usuario.correo === correo && usuario.password === password) {
                e.preventDefault();
                indiceUsuario = usuarios.indexOf(usuario);
                alert("El indice del usuario es: " + indiceUsuario);
            }
        }
        )
        if (indiceUsuario === 'false') {
            e.preventDefault();
            alert("No existe un usuario registrado con esas credenciales");
        }​
      limpiarFormulario(2);
    }
}
//Función para limpiar el formulario. Si es 1, limpia registro, si es 2 limpia login
function limpiarFormulario(formulario) {
    switch (formulario) {
        case 1: {
            document.getElementById("formRegistro").reset();
        }
        case 2: {
            document.getElementById("formLogin").reset();
        }
    }
}