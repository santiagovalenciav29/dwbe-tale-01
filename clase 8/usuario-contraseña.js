let Usuarios = [];

class Persona {
    constructor(nombre,apellido,email,pais,contrasenia,repetirContrasenia){
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.pais = pais;
        this.contrasenia = contrasenia;
        //this.repetirContrasenia = repetirContrasenia;
    }
};

document.getElementById("registrar").addEventListener('click', (e) => {

    e.preventDefault();

    const nombre = document.getElementById("nombre").value;
    const apellido = document.getElementById("apellido").value;
    const email =  document.getElementById("email").value;
    const pais = document.getElementById("pais").value;
    const contrasenia = document.getElementById("contrasenia").value;
    const repetirContrasenia = document.getElementById("repetirConstrasenia").value;
    
    if (Usuarios.findIndex(usuario => usuario.email === email) !== -1) {
        alert ('el correo ya existe')
    } else if (contrasenia !== repetirContrasenia) {
        alert ('las contraseñas no coinciden')
    } else {
        let persona = new Persona(nombre,apellido,email,pais,contrasenia);
        Usuarios.push(persona);
    }

    console.log(Usuarios)
});

document.getElementById("inicioSesion").addEventListener('click', (e) => {
    e.preventDefault();

    let emailIng =  document.getElementById("emailIng").value;
    let contraseniaIng = document.getElementById("contraseniaIng").value;

    let ingresar = Usuarios.findIndex(usuario => (usuario.email === emailIng && usuario.contrasenia === contraseniaIng));

    console.log(ingresar);

    if( ingresar === -1) {
        alert ('Usuario invalido')
    } else {
        alert ( `el usuario esta en la posicicon ${ingresar}`)
    }
});